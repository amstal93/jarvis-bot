package com.juliusdotsh.main;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;


@Configuration
@EnableJpaRepositories(basePackages = "com.juliusdotsh.repositories")
@EntityScan(basePackages = "com.juliusdotsh.entities")
@ComponentScan(basePackages = "com.juliusdotsh")
@ConfigurationProperties("application.properties")
@EnableScheduling
public class ApplicationConfig {
}
