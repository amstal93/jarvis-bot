package com.juliusdotsh.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name="USER")
public class User {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "telegram_id")
    private long telegramId;

    @Column(name = "name")
    private String name;

    @Column(name = "api_user")
    private String apiUser;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "state")
    private int state;

    @Column(name = "cron")
    private boolean cron;

    @Column(name = "language")
    private String language;
}
