package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import com.juliusdotsh.utility.KeyboardManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class LanguageMenu implements Command {

    private JarvisBotService jarvisBotService;
    private UserRepository userRepository;

    @Autowired
    public LanguageMenu(JarvisBotService jarvisBotService, UserRepository userRepository) {
        this.jarvisBotService = jarvisBotService;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void onExec(Message message) {
        User user = userRepository.findByTelegramId(message.getChatId());
        SendMessage sendMessage = new SendMessage()
                .enableMarkdown(true)
                .setChatId(user.getTelegramId())
                .setReplyMarkup(KeyboardManager.getLanguageKeyboard(user))
                .setText(LanguageManager.getLabel(user, "message.command.change.language"));
        jarvisBotService.execute(sendMessage);
        user.setState(3);
        userRepository.save(user);
    }
}
