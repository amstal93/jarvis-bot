package com.juliusdotsh.commands;

import com.juliusdotsh.components.Habitica;
import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.Task;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.Calendar;
import java.util.List;

@Component
public class DailyTasks implements Command {

	private JarvisBotService jarvisBotService;
	private UserRepository userRepository;
	private Habitica habitica;
	private Logger log = LoggerFactory.getLogger(DailyTasks.class);

	@Autowired
	public DailyTasks(JarvisBotService jarvisBotService, UserRepository userRepository, Habitica habitica) {
		this.jarvisBotService = jarvisBotService;
		this.userRepository = userRepository;
		this.habitica = habitica;
	}

	@Override
	public void onExec(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		if (!getUserDailyTasks(user)) {
            jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "error.habitica.communication"));
		}
	}

	public boolean getUserDailyTasks(User user) {
		if (user.getApiUser() != null && user.getApiKey() != null) {
			try {
				List<Task> tasks = habitica.getUserTask(user.getApiUser(), user.getApiKey());
				if (tasks.size() > 0) {
					boolean userIsFree = true;
					for (Task t : tasks) {
						if ("todo".equals(t.getType()) && null != t.getDate()) {
							int result = t.getDate().compareTo(Calendar.getInstance().getTime());
							if (result < 1) {
                                jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "message.habitica.task.not.completed") + t.getText());
							}
						} else if ("daily".equals(t.getType())) {
							if (BooleanUtils.isFalse(t.getCompleted()) && BooleanUtils.isTrue(t.getIsDue())) {
                                jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "message.habitica.task.not.completed") + t.getText());
								userIsFree = false;
							}
						}
					}
					if (userIsFree) {
                        jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "message.habitica.task.completed"));
					}
				} else {
                    jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "message.habitica.task.not.found"));
				}
				return true;
			} catch (Exception e) {
				log.info("Unable to get User task", e);
				return false;
			}
		}
		return false;
	}
}
