package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ApiKey implements Command {

	private JarvisBotService jarvisBotService;
	private UserRepository userRepository;

	public ApiKey(JarvisBotService jarvisBotService, UserRepository userRepository) {
		this.jarvisBotService = jarvisBotService;
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public void onExec(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		Pattern patternForApiKey = Pattern.compile("^[0-z]{8}-[0-z]{4}-[0-z]{4}-[0-z]{4}-[0-z]{12}$");
		Matcher matcherForApiKey = patternForApiKey.matcher(message.getText());
		if (matcherForApiKey.find()) {
			user.setState(0);
			user.setApiKey(message.getText().trim());
            user.setLanguage("en");
			userRepository.save(user);
            jarvisBotService.sendMessageHome(message.getChatId(), LanguageManager.getLabel(user, "message.command.api.key.success"), user);
		} else {
            jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "error.api.invalid.pattern"));
		}
	}
}
