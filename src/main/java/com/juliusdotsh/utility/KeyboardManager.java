package com.juliusdotsh.utility;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class KeyboardManager {
    public static ReplyKeyboardMarkup getHomeKeyboard(User user) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.habitica.task")));
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.settings")));
        keyboard.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    public static ReplyKeyboardMarkup getSettingsKeyboard(User user) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        if (user.isCron()) {
            keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.cron.disable")));
        }else{
            keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.cron.enable")));
        }
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.set.api")));
        keyboard.add(keyboardRow);
        keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.change.language")));
        keyboard.add(keyboardRow);
        keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.back")));
        keyboard.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    public static ReplyKeyboardMarkup getCancelKeyboard(User user) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "command.cancel")));
        keyboard.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    public static ReplyKeyboardMarkup getLanguageKeyboard(User user) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "language.it")));
        keyboardRow.add(new KeyboardButton().setText(LanguageManager.getLabel(user, "language.en")));
        keyboard.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }
}
