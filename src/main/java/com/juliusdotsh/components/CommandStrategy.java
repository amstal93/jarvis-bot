package com.juliusdotsh.components;

import com.juliusdotsh.commands.*;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandStrategy {

	private Settings settings;
	private Start start;
	private SetApi setApi;
	private DailyTasks dailyTask;
	private DisableCron disableCron;
	private EnableCron enableCron;
    private LanguageMenu languageMenu;

	@Autowired
    public CommandStrategy(Settings settings, Start start, SetApi setApi, DailyTasks dailyTask, DisableCron disableCron, EnableCron enableCron, LanguageMenu languageMenu) {
		this.settings = settings;
		this.start = start;
		this.setApi = setApi;
		this.dailyTask = dailyTask;
		this.disableCron = disableCron;
		this.enableCron = enableCron;
        this.languageMenu = languageMenu;
    }

    Command findCommandByMessageText(String text, User user) {
        Command command = checkForCancel(text, user);
        if (LanguageManager.getLabel(user, "command.settings").equals(text)) {
            command = settings;
        } else if (LanguageManager.getLabel(user, "command.back").equals(text) || "/start".equals(text)) {
            command = start;
        } else if (LanguageManager.getLabel(user, "command.set.api").equals(text)) {
            command = setApi;
        } else if (LanguageManager.getLabel(user, "command.habitica.task").equals(text)) {
            command = dailyTask;
        } else if (LanguageManager.getLabel(user, "command.cron.disable").equals(text)) {
            command = disableCron;
        } else if (LanguageManager.getLabel(user, "command.cron.enable").equals(text)) {
            command = enableCron;
        } else if (LanguageManager.getLabel(user, "command.change.language").equals(text)) {
            command = languageMenu;
		}
		return command;
	}

    Command checkForCancel(String text, User user) {
        if (LanguageManager.getLabel(user, "command.cancel").equals(text)) {
			return settings;
		}
		return null;
	}


}
